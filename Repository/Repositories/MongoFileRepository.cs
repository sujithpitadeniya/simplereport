﻿using Repository.Repositories.Interfaces;
using MongoDB.Bson;
using Repository.UnitOfWork;
using Data.ConnectorInterfaces;

namespace Repository.Repositories
{
    public class MongoFileRepository : MongoRepository<BsonDocument>, IMongoFileRepository
    {
        private readonly IMongoDBSettings mongoDBSettings;
        public MongoFileRepository(IMongoDBSettings mongoDBSettings):base(mongoDBSettings)
        {
            this.mongoDBSettings = mongoDBSettings;
        }
    }
}
