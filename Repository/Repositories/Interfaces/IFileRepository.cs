﻿using Data.Connector;
using Data.Entities;
using Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories.Interfaces
{
    public interface IFileRepository : IRepository<BIApiContext, Document>
    {
        string StoreFile(string fileName);

    }
}
