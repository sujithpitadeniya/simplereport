﻿using Data.ConnectorInterfaces;
using MongoDB.Bson;
using Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Repositories.Interfaces
{
    public interface IMongoFileRepository : IMongoRepository<BsonDocument>
    {
    }
}
