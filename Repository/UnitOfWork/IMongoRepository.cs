﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.UnitOfWork
{
    public interface IMongoRepository<T> where T : BsonDocument
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Get(string collectionName);
        T Get(ObjectId id, string collectionName);
        T Create(string collectionName, T p); 
        T Replace(string collectionName, T p);
        void Delete(ObjectId id, string collectionName);
        void Delete(string collectionName, T p);
        void Update(ObjectId id, string collectionName, T p);
    }
}
