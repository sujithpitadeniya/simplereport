﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using Data.ConnectorInterfaces;
using MongoDB.Driver;

namespace Repository.UnitOfWork
{
    public class MongoRepository<T> : IMongoRepository<T> where T : BsonDocument
    {

        private MongoClient _client;
        private MongoServer _server;
        private IMongoDatabase database;

        public MongoRepository(IMongoDBSettings mongoDBSettings)
        {
            _client = mongoDBSettings.GetMongoClient();
            _server = mongoDBSettings.GetMongoServer();
            database = mongoDBSettings.GetMongoDatabase();
        }
            public IEnumerable<T> GetAll()
        {
            //var docs = _db.GetCollection<MDocument>("Documents").FindAll();
            //var docs2 = _db.GetCollection<T>("Documents").FindAll();

            //return _db.GetCollection<T>("Documents").FindAll();
             var docs  = this.database.GetCollection<T>(typeof(T).Name).Find(new BsonDocument()).ToListAsync().Result;
            return docs;
        }

        public IEnumerable<T> Get(string collectionName)
        {
            //var docs = _db.GetCollection<MDocument>("Documents").FindAll();
            //var docs2 = _db.GetCollection<T>("Documents").FindAll();

            //return _db.GetCollection<T>("Documents").FindAll();
            var docs = this.database.GetCollection<T>(collectionName).Find(new BsonDocument()).ToListAsync().Result;
            return docs;
        }
        public T Get(ObjectId id, string collectionName)
        {
            //var query_id = Query.EQ("_id", id);
            //var entity = dbCollection.FindOne(query_id);

            var doc = this.database.GetCollection<T>(collectionName).Find(x => x.AsObjectId.Equals(id)).FirstOrDefaultAsync().Result;
            return doc;


            //var collection = database.GetCollection<T>(collectionName);
            //var query = Query.EQ("_id", id);
            //var entity = collection.FindOne(query);

            //var doc = this.database.GetCollection<T>(collectionName).Find(x => x.AsObjectId.Equals(id)).FirstOrDefaultAsync().Result;
            // return doc;

            //var res = Query<T>.EQ(p => p.AsObjectId, id);
            //return _db.GetCollection<T>("Documents").FindOne(res);
        }

        public T Create(string collectionName, T p)
        {
            var collection = this.database.GetCollection<T>(collectionName);
            //collection.InsertOneAsync(p);

            var result = (T)collection.InsertOneAsync(p).ToBsonDocument();
            return result;
            //_db.GetCollection<T>("Documents").Save(p);
            //return p;
        }

        public T Replace(string collectionName, T p)
        {
            var collection = this.database.GetCollection<T>(collectionName);

            collection.ReplaceOneAsync(x => x.AsObjectId.Equals(p.AsObjectId), p, new UpdateOptions
            {
                IsUpsert = true
            });

            return p;

            //_db.GetCollection<T>("Documents").Save(p);
            //return p;
        }

        public void Delete(ObjectId id, string collectionName)
        {
            var collection = this.database.GetCollection<T>(collectionName);

            collection.DeleteOneAsync(x => x.AsObjectId.Equals(id));
        }

        public void Delete(string collectionName, T entity)
        {
            var collection = this.database.GetCollection<T>(collectionName);

            collection.DeleteOneAsync(x => x.AsObjectId.Equals(entity.AsObjectId));
        }

        public void Update(ObjectId id, string collectionName, T p)
        {
            var collection = this.database.GetCollection<T>(collectionName);

            var filter = Builders<T>.Filter.Eq(s => s.AsObjectId, id);
            var result = collection.ReplaceOneAsync(filter, p).Result;

            collection.ReplaceOneAsync(x => x.AsObjectId.Equals(id), p, new UpdateOptions
            {
                IsUpsert = true
            });

            //var res = Query<T>.EQ(pd => pd.AsObjectId, id);
            //var operation = Update<T>.Replace(p);
            //_db.GetCollection<T>("Documents").Update(res, operation);
        }

        //public void Remove(ObjectId id)
        //{
        //    var res = Query<T>.EQ(e => e.AsObjectId, id);
        //    var operation = _db.GetCollection<T>("Documents").Remove(res);
        //}
    }
}
