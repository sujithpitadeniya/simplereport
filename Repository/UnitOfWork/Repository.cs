﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UnitOfWork
{
    /// <summary>
    /// Class Repository.
    /// </summary>
    /// <typeparam name="CContext">The type of the c context.</typeparam>
    /// <typeparam name="TEntity">The type of the t entity.</typeparam>
    /// <seealso cref="Flexie.Data.UnitOfWork.IGenericRepository{CContext, TEntity}" />
    public class Repository<CContext, TEntity> : IRepository<CContext, TEntity>
                                                    where TEntity : BaseEntity
                                                    where CContext : DbContext, new()
    {
        private CContext _dbContext;
        private DbSet<TEntity> _dbSet;

        public Repository(CContext dbContext)
        {
            this._dbContext = dbContext;
            this._dbSet = dbContext.Set<TEntity>();
        }

        /// <summary>
        /// Gets the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="includeProperties">The include properties. not implemented since lazy loading not yet support ef core</param>
        /// <returns>IQueryable&lt;TEntity&gt;.</returns>
        public async Task<IQueryable<TEntity>> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
                query = query.Where(filter);

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }


            return (orderBy != null) ? orderBy(query) : query;
        }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TEntity.</returns>
        public async Task<TEntity> GetById(Int64 id, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return await query.SingleOrDefaultAsync(i => i.Id == id);
        }

        /// <summary>
        /// Inserts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <exception cref="System.ArgumentNullException">entity</exception>
        public async Task<TEntity> Insert(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            await this._dbSet.AddAsync(entity);
            await this._dbContext.SaveChangesAsync();

            return entity;
        }

        /// <summary>
        /// Deletes the specified entity to delete.
        /// </summary>
        /// <param name="entityToDelete">The entity to delete.</param>
        /// <exception cref="System.ArgumentNullException">entity</exception>
        public async Task Delete(TEntity entityToDelete)
        {
            if (entityToDelete == null)
                throw new ArgumentNullException("entity");

            this._dbSet.Remove(entityToDelete);
            await this._dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Updates the specified entity to update.
        /// </summary>
        /// <param name="entityToUpdate">The entity to update.</param>
        /// <exception cref="System.ArgumentNullException">entity</exception>
        public async Task<TEntity> Update(TEntity entityToUpdate)
        {
            if (entityToUpdate == null)
                throw new ArgumentNullException("entity");

            this._dbSet.Update(entityToUpdate);
            await this._dbContext.SaveChangesAsync();

            return entityToUpdate;
        }
    }
}
