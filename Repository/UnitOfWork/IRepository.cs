﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.UnitOfWork
{
    /// <summary>
    /// Interface IRepository
    /// </summary>
    /// <typeparam name="CContext">The type of the c context.</typeparam>
    /// <typeparam name="TEntity">The type of the t entity.</typeparam>
    public interface IRepository<CContext, TEntity> where TEntity : BaseEntity
                                                            where CContext : DbContext, new()
    {
        /// <summary>
        /// Gets the specified filter.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="orderBy">The order by.</param>
        /// <param name="includeProperties">The include properties.</param>
        /// <returns>IQueryable&lt;TEntity&gt;.</returns>
        Task<IQueryable<TEntity>> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>TEntity.</returns>
        Task<TEntity> GetById(Int64 id, string includeProperties = "");

        /// <summary>
        /// Inserts the specified entity.
        /// </summary>
        /// <param name="entity">The entity.</param>
        Task<TEntity> Insert(TEntity entity);

        /// <summary>
        /// Deletes the specified entity to delete.
        /// </summary>
        /// <param name="entityToDelete">The entity to delete.</param>
        Task Delete(TEntity entityToDelete);

        /// <summary>
        /// Updates the specified entity to update.
        /// </summary>
        /// <param name="entityToUpdate">The entity to update.</param>
        Task<TEntity> Update(TEntity entityToUpdate);
    }

}
