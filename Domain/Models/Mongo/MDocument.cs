﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Models.Mongo
{
    public class MDocument
    {
        public ObjectId Id { get; set; }
        [BsonElement("DocuemntId")]
        public int DocuemntId { get; set; }
        [BsonElement("FileName")]
        public string FileName { get; set; }
        [BsonElement("Extension")]
        public int Extension { get; set; }
        [BsonElement("Url")]
        public string Url { get; set; }
    }
}
