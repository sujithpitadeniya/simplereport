﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Data.Connector;
using Domain.Models.Mongo;

namespace BIApi2.Controllers
{
    [Produces("application/json")]
    [Route("api/Document")]
    public class DocumentApiController : Controller
    {
        public DocumentApiController()
        {

        }

        [HttpGet]
        public IEnumerable<MDocument> Get()
        {
            return null;
        }

        [HttpGet("{id}", Name = "GetDocument")]
        public IActionResult Get(string id)
        {
            return new OkResult();
        }

        [HttpPost]
        public IActionResult Post([FromBody]MDocument p)
        {

            return new OkObjectResult(p);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Put(string id, [FromBody]MDocument p)
        {
            return new OkResult();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            return new OkResult();
        }
    }
}
