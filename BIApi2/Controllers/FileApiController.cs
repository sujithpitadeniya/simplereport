﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;
using System.Web.Http.Cors;
using Service.Services.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using MongoDB.Driver;
using Data.ConnectorInterfaces;
using MongoDB.Bson;
using Newtonsoft.Json;
using System.Data;
using Data.Adaptor;
using Microsoft.Extensions.Configuration;
using Data.Reader;
using Data.Connector;

namespace BIApi2.Controllers
{
    [Produces("application/json")]
    [Route("api/File")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FileApiController : Controller
    {
        private IConfiguration configuration;
        private IHostingEnvironment hostingEnvironment;
        private IFileService iFileService { get; set; }
        private IMongoDbFileService iMongoDbFileService { get; set; }
        private IDataService iDataService { get; set; }
        public FileApiController(IHostingEnvironment _hostingEnvironment, IConfiguration _configuration, IFileService iFileService,
            IMongoDbFileService iMongoDbFileService, IDataService iDataService)
        {
            hostingEnvironment = _hostingEnvironment;
            configuration = _configuration;
            this.iFileService = iFileService;
            this.iMongoDbFileService = iMongoDbFileService;
            this.iDataService = iDataService;
        }

        [HttpGet("GetAll")]
        public JsonResult GetAll()
        {
            var items = iMongoDbFileService.GetAll();
            var jsonString = items.ToJson();

            return Json(new { result = jsonString });
        }

        // GET: api/FileApi
        [HttpGet("GetCollection")]
        public JsonResult GetCollection(string collectionName)
        {
            var items = iMongoDbFileService.Get(collectionName);
            var jsonString = items.ToJson();

            return Json(new { result = jsonString });
        }

        // GET: api/FileApi/5
        [HttpGet("GetFile")]
        public JsonResult GetFile(string id, string collecitonName)
        {
            try
            {
                var objectId = new ObjectId(id);
                var item = iMongoDbFileService.Get(objectId, collecitonName);
                return Json(new { result = item });
            }
            catch (Exception ex)
            {
                return Json(new { result = "Invalid Object id" });
            }
        }

        // POST: api/FileApi
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/FileApi/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpPost("Upload")]
        public async Task<JsonResult> FileUpload()
        {
            try
            {
                var file = Request.Form.Files[0];
                var folderName = "uploads";
                var webRootPath = hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                    string fullPath = Path.Combine(newPath, fileName);
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                }
                return Json("Upload Successful.");
            }
            catch (Exception ex)
            {
                return Json("Upload Failed: " + ex.Message);
            }
        }

        //Read CSV file
        [HttpPost("UploadDataFile")]
        public async Task<JsonResult> UploadDataFile()
        {
            var contentTypes = new List<string>();
            string message = "";
            try
            {
                string collectionName = Request.Form["collectionName"];
                string existingTableName = Request.Form["existingTableName"];
                string replaceData = Request.Form["marked"];
                bool replace = false;
                replace = replaceData == "true";

                if (!string.IsNullOrEmpty(collectionName))
                {
                    if (!string.IsNullOrWhiteSpace(existingTableName))
                    {
                        if (existingTableName != "undefined")
                            collectionName = existingTableName;
                    }

                    var tables = iDataService.GetTableNames(string.Empty);
                    bool tablExists = tables.Any(x => x.Equals(collectionName));
                    if (tablExists)
                        existingTableName = collectionName;
                    if(Request.Form.Files.Count > 0)
                    {
                        foreach (var file in Request.Form.Files)
                        {
                            //var fileContent = Request.Form.Files[file.FileName];
                            if (file != null && file.Length > 0)
                            {
                                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                                contentTypes.Add(file.ContentType);

                                var inputStream = file.OpenReadStream();
                                // invalidItems = await ReadStream(file.OpenReadStream());
                                ICSVDataReader dataReader = new CSVDataReader(configuration);
                                var result = await dataReader.ReadStream(file.OpenReadStream(), collectionName, tablExists, replace);
                                if (result)
                                    message = "File uploaded successfuly";
                                else
                                    message = "Error in file uploading";
                            }
                        }
                    }
                    else
                        message = "Please upload a file.";
                }

                if (string.IsNullOrWhiteSpace(existingTableName))
                {
                    message = "Table Name is required";
                }

                return Json(new { message });
            }
            catch (Exception ex)
            {
                return Json(new { message = "Upload Failed: " + ex.Message });
            }
        }

        // Read Data Service Url
        [HttpPost("GetDataServiceUrl")]
        public async Task<JsonResult> GetDataServiceUrl()
        {
            var contentTypes = new List<string>();
            string collectionName = Request.Form["collectionName"];
            string serviceUrl = Request.Form["serviceUrl"];
            string existingTableName = Request.Form["existingTableName"];
            string replaceData = Request.Form["marked"];
            bool replace = false;
            replace = replaceData == "true";

            string message = "";

            if (string.IsNullOrEmpty(collectionName) && string.IsNullOrEmpty(existingTableName))
            {
                message = "Table Name required";
                return Json(new { message });
            }

            if (collectionName == "undefined" && existingTableName == "undefined")
            {
                message = "Table Name required";
                return Json(new { message });
            }

            if (!string.IsNullOrEmpty(collectionName))
            {
                if (!string.IsNullOrEmpty(existingTableName))
                {
                    if (existingTableName != "undefined")
                        collectionName = existingTableName;
                    else if (collectionName == "undefined")
                    {
                        message = "Table Name required";
                        return Json(new { message });
                    }
                }
            }

            var tables = iDataService.GetTableNames(string.Empty);
            bool tablExists = tables.Any(x => x.Equals(collectionName));
            if (tablExists)
                existingTableName = collectionName;

            if (!string.IsNullOrEmpty(serviceUrl) && serviceUrl != "undefined")
            {
                IWebApiDataReader dataReader = new WebApiDataReader(configuration);
                var result = await dataReader.ReadServiceData(null, serviceUrl, collectionName, tablExists, replace);
                if (result)
                    message = "Successfuly read data";
                else
                    message = "Failure in reading data";
            }
            else
                message = "Please provide a url.";

            return Json(new { message });

        }

        // Read Data Service Url
        [HttpPost("ConnectToDb")]
        public JsonResult ConnectToDb()
        {
            var contentTypes = new List<string>();
            string collectionName = Request.Form["collectionName"];
            string existingTableName = Request.Form["existingTableName"];
            string serviceUrl = Request.Form["serviceUrl"];
            string replaceData = Request.Form["marked"];

            string serverName = Request.Form["serverName"];
            string dbName = Request.Form["dbName"];
            string userName = Request.Form["userName"];
            string password = Request.Form["password"];
            bool replace = false;
            replace = replaceData == "true";

            var tables = iDataService.GetTableNames(string.Empty);
            bool tablExists = tables.Any(x => x.Equals(collectionName));
            if (tablExists)
                existingTableName = collectionName;

            List<string> tableNames = new List<string>();

            SQLConnector connector = new SQLConnector(configuration);
            var con = connector.ConnectToDB(serverName, dbName, userName, password);
            DataTable dt = new DataTable();
            dt = con.GetSchema("Tables"); ;
  
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string tablename = (string)row[2];
                    tableNames.Add(tablename);
                }
            }

            return Json(tableNames);
        }

        // Read external database, and store data
        [HttpPost("StoreData")]
        public async Task<JsonResult> StoreData()
        {
            var contentTypes = new List<string>();
            string collectionName = Request.Form["collectionName"];
            string existingTableName = Request.Form["existingTableName"];
            string externalSourceTableName = Request.Form["externalSourceTableName"];
            string serviceUrl = Request.Form["serviceUrl"];
            string replaceData = Request.Form["marked"];

            string serverName = Request.Form["serverName"];
            string dbName = Request.Form["dbName"];
            string userName = Request.Form["userName"];
            string password = Request.Form["password"];
            bool replace = false;
            replace = replaceData == "true";

            string message = "";

            if (string.IsNullOrEmpty(collectionName) && string.IsNullOrEmpty(existingTableName))
            {
                message = "Table Name required";
                return Json(new { message });
            }

            if (collectionName == "undefined" && existingTableName == "undefined")
            {
                message = "Table Name required";
                return Json(new { message });
            }

            if (!string.IsNullOrEmpty(collectionName))
            {
                if (!string.IsNullOrEmpty(existingTableName))
                {
                    if (existingTableName != "undefined")
                        collectionName = existingTableName;
                    else if (collectionName == "undefined")
                    {
                        message = "Table Name required";
                        return Json(new { message });
                    }
                }
            }

            var tables = iDataService.GetTableNames(string.Empty);
            bool tablExists = tables.Any(x => x.Equals(collectionName));
            if (tablExists)
                existingTableName = collectionName;

            List<string> tableNames = new List<string>();

            if (!string.IsNullOrEmpty(externalSourceTableName) && externalSourceTableName != "undefined")
            {
                ISQLDataReader dataReader = new SQLDataReader(configuration);
                SQLConnector connector = new SQLConnector(configuration);
                var con = connector.ConnectToDB(serverName, dbName, userName, password);
                var result = await dataReader.StoreData(con, externalSourceTableName, collectionName, tablExists, replace);

                if (result)
                    message = "External Source Table data stored successfuly.";
                else
                    message = "Error in storing table data.";
            }
            else
                message = "External Source Table is required.";

            return Json(new { message });
        }

        public async Task<JsonResult> ReadStream(Stream stream, string tableName, bool isTableExists)
        {
            try
            {
                string csvDelimeter = ",";
                var reader = new StreamReader(stream);
                var line = await reader.ReadLineAsync();
                var columnNames = Regex.Split(line, csvDelimeter);
                DataTable dt = new DataTable();
                Dictionary<string, string> columnsWithTypes = new Dictionary<string, string>();
                IDataSourceAdaptor iCsvAdaptor = new CSVAdaptor(new Data.Reader.CSVDataReader(configuration));

                for (int j = 0; j < columnNames.Length; j++)
                {
                    columnsWithTypes.Add(columnNames[j], "VARCHAR(250)");
                    dt.Columns.Add(columnNames[j], typeof(string));
                }

                dt.Columns.Add("CreatedDate", typeof(DateTime));

                int i = 0;
                while (!reader.EndOfStream)
                {
                    //line = reader.ReadLine();
                    //var values = line.Split(csvDelimeter.ToCharArray()).ToList();

                    while (!string.IsNullOrEmpty(line = await reader.ReadLineAsync()))
                    {
                        string[] cols = Regex.Split(line, csvDelimeter);
                        DataRow row = dt.NewRow();

                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            //columnsWithTypes.Add(columnNames[j], "VARCHAR(100)");
                            //dt.Columns.Add("TI", typeof(string));
                            row[j] = cols[j];
                        }
                        row[columnNames.Length + 1] = DateTime.Now;
                        dt.Rows.Add(row);
                    }

                    i++;
                }

                await iCsvAdaptor.CreatData(tableName, columnsWithTypes, dt, isTableExists, false);
                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return Json(new { success = false, msg = message });
            }
        }

        [HttpGet("GetTableNames")]
        public JsonResult GetTableNames(ICollection<IFormFile> files)
        {
            List<string> tables = new List<string>();
            tables = iDataService.GetTableNames(string.Empty);

            return Json(tables);
        }

        [HttpGet("GetDataTable")]
        public JsonResult GetDataTable(string tableName)
        {
            List<string> tables = new List<string>();
            DataTable dt = iDataService.GetDataTable(string.Empty, tableName);

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                     .Select(x => x.ColumnName)
                     .ToArray();
            List<Object> obj = new List<Object>();
            foreach (DataRow row in dt.Rows)
            {
                obj.Add(row);
                foreach (DataColumn column in dt.Columns)
                {
                    Console.WriteLine(row[column]);
                }
            }

            return Json(obj);
        }

        public async Task<JsonResult> ReadStreamForMongoDB(Stream stream, string tableName)
        {
            try
            {
                string csvDelimeter = ",";
                var reader = new StreamReader(stream);
                var line = await reader.ReadLineAsync();
                var columnNames = Regex.Split(line, csvDelimeter);

                int i = 0;
                while (!reader.EndOfStream)
                {
                    line = reader.ReadLine();
                    var values = line.Split(csvDelimeter.ToCharArray()).ToList();

                    while (!string.IsNullOrEmpty(line = await reader.ReadLineAsync()))
                    {
                        string[] cols = Regex.Split(line, ",");
                        BsonDocument book = new BsonDocument();
                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            book.Add(columnNames[j], cols[j]);
                        }

                        iMongoDbFileService.Create(tableName, book);
                    }

                    // Insert as bulk of records
                    //List<BsonDocument> batch = new List<BsonDocument>();
                    //while (!string.IsNullOrEmpty(line = await reader.ReadLineAsync()))
                    //{
                    //    string[] cols = Regex.Split(line, ",");
                    //    BsonDocument book = new BsonDocument();
                    //    for (int j = 0; j < columnNames.Length; j++)
                    //    {
                    //        book.Add(columnNames[j], cols[j]);


                    //    }
                    //    batch.Add(book);
                    //}

                    //await iMongoDbFileService.InsertManyAsync(batch.AsEnumerable());

                    i++;
                }

                return Json(new { success = true });
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return Json(new { success = false, msg = message });
            }
        }
    }
}
