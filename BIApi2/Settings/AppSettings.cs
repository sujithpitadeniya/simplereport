﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIApi2.Settings
{
    public class AppSettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
    }
    public class ConnectionStrings
    {
        public string Test { get; set; }
        public string Uat { get; set; }
        public string Prod { get; set; }
    }
}
