﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BIApi2.Models
{
    public class Document
    {
        public ObjectId Id { get; set; }
        [BsonElement("DocuemntId")]
        public int DocuemntId { get; set; }
        [BsonElement("FileName")]
        public string FileName { get; set; }
        [BsonElement("Extension")]
        public int Extension { get; set; }
        [BsonElement("Url")]
        public string Url { get; set; }
    }
}
