﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Web.Http;
using Data.Connector;
using Service.Services.Interfaces;
using Service.Services;
using Microsoft.EntityFrameworkCore;
using Data.ConnectorInterfaces;
using Repository.Repositories.Interfaces;
using Repository.Repositories;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite;
using BIApi2.Settings;
using Data.Reader;
using Data.Adaptor;

namespace BIApi2
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
               .SetBasePath(env.ContentRootPath)
               .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
               .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            //if (env.IsDevelopment())
            //{
            //    // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
            //    builder.AddUserSecrets<Startup>();
            //}

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            //services.AddCors();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddSingleton(Configuration);

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });

            //services.Configure(options =>
            //{
            //    options.Filters.Add(new RequireHttpsAttribute());
            //});

            // Shows UseCors with CorsPolicyBuilder.

            //Configure dbcontext for sql connector
            services.AddDbContext<BIApiContext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            //DI
            services.AddTransient<MongoDbDataConnector>();
            //connector
            services.AddTransient<IMongoDBSettings, MongoDbDataConnector>();

            //repositories
            services.AddTransient<IFileRepository, FileRepository>();
            services.AddTransient<IMongoFileRepository, MongoFileRepository>();

            //services
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IMongoDbFileService, MongoDbFileService>();
            services.AddTransient<IDataService, DataService>();
            
            services.AddTransient<IDataReader, CSVDataReader>();
            services.AddTransient<IDataReader, SQLDataReader>();
            services.AddTransient<IDataReader, WebApiDataReader>();

            services.AddTransient<IDataSourceAdaptor, WebApiAdaptor>();
            services.AddTransient<IDataSourceAdaptor, SQLAdaptor>();
            services.AddTransient<IDataSourceAdaptor, CSVAdaptor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    //app.UseHsts();
            //}

            //var options = new RewriteOptions().AddRedirectToHttps(StatusCodes.Status301MovedPermanently, 63423);
            //app.UseRewriter(options);

            ////app.UseCors(builder => builder.WithOrigins("http://localhost:4200").AllowAnyOrigin());
            //app.UseMvc();

            ////app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseSpaStaticFiles();
            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(name: "default", template: "{controller}/{action=index}/{id}");
            //});

            //app.UseSpa(spa =>
            //{
            //    // To learn more about options for serving an Angular SPA from ASP.NET Core,
            //    // see https://go.microsoft.com/fwlink/?linkid=864501

            //    spa.Options.SourcePath = "ClientApp";

            //    if (env.IsDevelopment())
            //    {
            //        spa.UseAngularCliServer(npmScript: "start");
            //    }
            //});


            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseMvc();

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
