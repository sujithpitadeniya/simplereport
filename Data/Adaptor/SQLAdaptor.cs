﻿using Data.Reader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Data.Adaptor
{
    public class SQLAdaptor : IDataSourceAdaptor
    {
        private SQLDataReader sQLDataReader { get; set; }
        public SQLAdaptor(SQLDataReader _sQLDataReader) => sQLDataReader = _sQLDataReader;
        public async Task<bool> CreatData(string tableName, Dictionary<string, string> columns, DataTable dataTable, bool isTableExists =false, bool replace = false)
        {
            return await sQLDataReader.CreateData(tableName, columns, dataTable, isTableExists, replace);
        }

        public async Task<bool> GetData()
        {
            return await sQLDataReader.GetData();
        }
    }
}
