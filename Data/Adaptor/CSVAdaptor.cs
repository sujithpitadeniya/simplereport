﻿using Data.Reader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Data.Adaptor
{
    public class CSVAdaptor : IDataSourceAdaptor
    {
        private CSVDataReader CSVDataReader { get; set; }
        public CSVAdaptor(CSVDataReader _cSVDataReader) => CSVDataReader = _cSVDataReader;
        public async Task<bool> CreatData(string tableName, Dictionary<string, string> columns, DataTable dataTable, bool isTableExists = false, bool replace = false)
        {
            return await CSVDataReader.CreateData(tableName, columns, dataTable, isTableExists, replace);
        }
        public async Task<bool> GetData()
        {
            return await CSVDataReader.GetData();
        }     
    }
}
