﻿using Data.Reader;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Data.Adaptor
{
    public class WebApiAdaptor : IDataSourceAdaptor
    {
        private WebApiDataReader webApiDataReader { get; set; }
        public WebApiAdaptor(WebApiDataReader _webApiDataReader) => webApiDataReader = _webApiDataReader;

        public async Task<bool> CreatData(string tableName, Dictionary<string, string> columns, DataTable dataTable, bool isTableExists = false, bool replace = false)
        {
            return await webApiDataReader.CreateData(tableName, columns, dataTable, isTableExists, replace);
        }

        public async Task<bool> GetData()
        {
            return await webApiDataReader.GetData();
        }
    }
}
