﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Data.Adaptor
{
    public interface IDataSourceAdaptor
    {
        Task<bool> CreatData(string tableName, Dictionary<string, string> columns, DataTable dataTable, bool isTableExists, bool replace);
        Task<bool> GetData();
    }
}
