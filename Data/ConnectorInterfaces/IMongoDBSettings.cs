﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ConnectorInterfaces
{
    public interface IMongoDBSettings
    {
        MongoClient GetMongoClient();
        MongoServer GetMongoServer();
        IMongoDatabase GetMongoDatabase();
    }
}
