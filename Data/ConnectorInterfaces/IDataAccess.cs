﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.ConnectorInterfaces
{
    public interface IDataAccess<T> where T : class
    {
        IEnumerable<T> Get();

        T Get(ObjectId id);

        T Create(T p);

        void Update(ObjectId id, T p);

        void Remove(ObjectId id);
    }
}
