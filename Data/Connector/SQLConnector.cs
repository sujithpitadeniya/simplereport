﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Data.Connector
{
    public class SQLConnector
    {
        IConfiguration configuration;
        public SQLConnector(IConfiguration _iconfiguration)
        {
            configuration = _iconfiguration;
        }

        public SQLConnector()
        {

        }

        public SqlConnection GetConnectionByConnectionName(string connectionName)
        {
            //string cnstr = configuration.GetConnectionString(connectionName);
            string cnstr = configuration.GetConnectionString("DefaultConnection");
            //ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            SqlConnection cn = new SqlConnection(cnstr);
            cn.Open();
            return cn;
        }

        public SqlConnection GetConnectionByConnectionString(string connectionstring)
        {
            SqlConnection cn = new SqlConnection(connectionstring);
            cn.Open();
            return cn;
        }

        public static void Dispose(SqlConnection con)
        {
            if (con.State == System.Data.ConnectionState.Open)
                con.Close();
            con.Dispose();
        }

        public DataSet ExecuteProcedure(string connectionName, string storedProcName, Dictionary<string, SqlParameter> procParameters)
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = GetConnectionByConnectionName(connectionName))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = storedProcName;
                    // assign parameters passed in to the command
                    foreach (var procParameter in procParameters)
                    {
                        cmd.Parameters.Add(procParameter.Value);
                    }
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
            }
            return ds;
        }

        public DataSet ExecuteQuery(string connectionName, string query, Dictionary<string, SqlParameter> procParameters)
        {
            DataSet ds = new DataSet();
            using (SqlConnection cn = GetConnectionByConnectionName(connectionName))
            {
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    // assign parameters passed in to the command
                    foreach (var procParameter in procParameters)
                    {
                        cmd.Parameters.Add(procParameter.Value);
                    }
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }
            }

            return ds;
        }

        public async Task ExecuteNonQuery(string connectionName, string query)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = GetConnectionByConnectionName(connectionName))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    await cmd.ExecuteNonQueryAsync();
                    Dispose(con);
                }
            }
        }

        public async Task<int> ExecuteCommand(string connectionName, string storedProcName, Dictionary<string, SqlParameter> procParameters)
        {
            int rc;
            using (SqlConnection cn = GetConnectionByConnectionName(connectionName))
            {
                // create a SQL command to execute the stored procedure
                using (SqlCommand cmd = cn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = storedProcName;
                    // assign parameters passed in to the command
                    foreach (var procParameter in procParameters)
                    {
                        cmd.Parameters.Add(procParameter.Value);
                    }
                    rc = await cmd.ExecuteNonQueryAsync();
                }
            }
            return rc;
        }

        public async Task InsertBulkData(string connectionName, string tableName, DataTable dataTable, bool isTableExists = false, bool replace = false)
        {
            // connect to SQL
            using (SqlConnection con = GetConnectionByConnectionName(connectionName))
            {
                if (isTableExists)
                {
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(con))
                    {
                        bulkCopy.DestinationTableName = tableName;
                        try
                        {
                            // Write unchanged rows from the source to the destination.
                            if (replace)
                            {
                                //Truncate Existing Table
                                string truncateQuery = "TRUNCATE TABLE " + tableName;
                                await ExecuteNonQuery(connectionName, truncateQuery);

                                //replace data again
                                await bulkCopy.WriteToServerAsync(dataTable);
                            }
                            else // append data
                                await bulkCopy.WriteToServerAsync(dataTable, DataRowState.Added);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }
                }
                else
                {
                    // make sure to enable triggers
                    // more on triggers in next post
                    SqlBulkCopy bulkCopy = new SqlBulkCopy(con,
                    SqlBulkCopyOptions.TableLock | SqlBulkCopyOptions.FireTriggers | SqlBulkCopyOptions.UseInternalTransaction, null);

                    // set the destination table name
                    bulkCopy.DestinationTableName = tableName;

                    // write the data in the "dataTable"
                    try
                    {
                        // Write unchanged rows from the source to the destination.
                        await bulkCopy.WriteToServerAsync(dataTable);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                Dispose(con);
            }

            // reset
            dataTable.Clear();
        }

        public DataTable GetTableNames(string connectionName)
        {
            DataTable dt = new DataTable();
            // connect to SQL
            using (SqlConnection con = GetConnectionByConnectionName(connectionName))
            {
                dt = con.GetSchema("Tables"); ;
            }

            return dt;
        }

        public SqlConnection ConnectToDB(string serverName, string database, string userName, string password)
        {
            try
            {
                string connectionString =
                      "Data Source=" + serverName + ";" +
                      "Initial Catalog=" + database + ";" +
                      "User id=" + userName + ";" +
                      "Password=" + password + ";";

                SQLConnector connector = new SQLConnector();
                var con = connector.GetConnectionByConnectionString(connectionString);
                return con;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
