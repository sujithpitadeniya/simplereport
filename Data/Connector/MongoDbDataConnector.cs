﻿using Data.ConnectorInterfaces;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Connector
{
    public class MongoDbDataConnector : IMongoDBSettings
    {
        MongoClient _client;
        //MongoServer _server;
        IMongoDatabase _db;

        public MongoDbDataConnector()
        {
            _client = new MongoClient("mongodb://127.0.0.1:27017");
            //_server = _client.GetServer();
            _db = _client.GetDatabase("DocumentDB");
        }

        public MongoDbDataConnector(MongoClient client, MongoServer server, IMongoDatabase db)
        {
            _client = client;
            //_server = server;
            _db = db;
        }

        public MongoClient GetMongoClient()
        {
            return _client;
        }

        public MongoServer GetMongoServer()
        {
            return null;
        }

        public IMongoDatabase GetMongoDatabase()
        {
            return _db;
        }
    }
}
