﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Connector
{
    public class DbInitializer
    {
        public static void Initialize(BIApiContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
