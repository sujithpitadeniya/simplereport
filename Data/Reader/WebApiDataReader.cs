﻿using Data.Adaptor;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Data.Reader
{
    public class WebApiDataReader : DataReader, IWebApiDataReader
    {
        IConfiguration configuration;

        public WebApiDataReader(IConfiguration _configuration) : base(_configuration) => configuration = _configuration;

        public async Task<string> ReadData()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ReadServiceData(List<string> parameters, string serviceUrl, string tableName, bool isTableExists, bool replace)
        {
            Uri mainUrl = new Uri(serviceUrl);
            string baseUrl = mainUrl.Scheme + "://" + mainUrl.Authority;
            string apiUrl = mainUrl.AbsolutePath;
            bool success = false;

            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(baseUrl);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = await client.GetAsync(apiUrl);  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body.
                    var result = await response.Content.ReadAsAsync<List<JObject>>();
                    List<string> properties = new List<string>();
                    //List<string> propertyValues = new List<string>();

                    if (result != null)
                    {
                        DataTable dt;
                        Dictionary<string, string> columnsWithTypes;

                        ReadJSONList(result, properties, tableName,out dt, out columnsWithTypes);

                        IDataSourceAdaptor iWebApiAdaptor = new WebApiAdaptor(new WebApiDataReader(configuration));
                        success = await iWebApiAdaptor.CreatData(tableName, columnsWithTypes, dt, isTableExists, replace);
                    }
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                }

                //Make any other calls using HttpClient here.
                //Dispose once all HttpClient calls are complete. This is not necessary if the containing object will be disposed of; for example in this case the HttpClient instance will be disposed automatically when the application terminates so the following call is superfluous.
                client.Dispose();

                return success;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
                return success;
            }
        }

        private void ReadJSONList(List<JObject> result, List<string> properties, string tableName, out DataTable dt, out Dictionary<string, string> columnsWithTypes)
        {
            foreach (JProperty property in result[0].Properties())
            {
                Console.WriteLine(property.Name + " - " + property.Value);
                properties.Add(property.Name);
                // propertyValues.Add(property.Value.ToString());
            }

            dt = new DataTable();
            columnsWithTypes = new Dictionary<string, string>();

            dt.Columns.Add(tableName.Replace(" ", "_") + "_ID", typeof(int));

            for (int j = 0; j < properties.Count; j++)
            {
                columnsWithTypes.Add(properties[j], "VARCHAR(250)");
                dt.Columns.Add(properties[j], typeof(string));
            }
            dt.Columns.Add(tableName.Replace(" ", "_") + "_CreatedDate", typeof(DateTime));

            int i = 0;
            foreach (JObject o in result)
            {
                //string[] cols = Regex.Split(line, ",");
                DataRow row = dt.NewRow();
                int j = 0;
                foreach (string property in properties)
                {
                    row[j+1] = o[property];
                    j++;
                }

                row[properties.Count + 1] = DateTime.Now;
                dt.Rows.Add(row);
                i++;
            }

            Console.WriteLine(i);
        }

        //Get Fields names from a json object
        public static List<string> GetFieldNames(dynamic input)
        {
            List<string> fieldNames = new List<string>();

            try
            {
                // Deserialize the input json string to an object
                input = Newtonsoft.Json.JsonConvert.DeserializeObject(input);

                // Json Object could either contain an array or an object or just values
                // For the field names, navigate to the root or the first element
                input = input.Root ?? input.First ?? input;

                if (input != null)
                {
                    // Get to the first element in the array
                    bool isArray = true;
                    while (isArray)
                    {
                        input = input.First ?? input;

                        if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                        input.GetType() == typeof(Newtonsoft.Json.Linq.JValue) ||
                        input == null)
                            isArray = false;
                    }

                    // check if the object is of type JObject. 
                    // If yes, read the properties of that JObject
                    if (input.GetType() == typeof(Newtonsoft.Json.Linq.JObject))
                    {
                        // Create JObject from object
                        Newtonsoft.Json.Linq.JObject inputJson =
                            Newtonsoft.Json.Linq.JObject.FromObject(input);

                        // Read Properties
                        var properties = inputJson.Properties();

                        // Loop through all the properties of that JObject
                        foreach (var property in properties)
                        {
                            // Check if there are any sub-fields (nested)
                            // i.e. the value of any field is another JObject or another JArray
                            if (property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JObject) ||
                            property.Value.GetType() == typeof(Newtonsoft.Json.Linq.JArray))
                            {
                                // If yes, enter the recursive loop to extract sub-field names
                                var subFields = GetFieldNames(property.Value.ToString());

                                if (subFields != null && subFields.Count > 0)
                                {
                                    // join sub-field names with field name 
                                    //(e.g. Field1.SubField1, Field1.SubField2, etc.)
                                    fieldNames.AddRange(
                                        subFields
                                        .Select(n =>
                                        string.IsNullOrEmpty(n) ? property.Name :
                                     string.Format("{0}.{1}", property.Name, n)));
                                }
                            }
                            else
                            {
                                // If there are no sub-fields, the property name is the field name
                                fieldNames.Add(property.Name);
                            }
                        }
                    }
                    else
                        if (input.GetType() == typeof(Newtonsoft.Json.Linq.JValue))
                    {
                        // for direct values, there is no field name
                        fieldNames.Add(string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in reading json values: " + ex.Message);
                throw;
            }

            return fieldNames;
        }
    }
}
