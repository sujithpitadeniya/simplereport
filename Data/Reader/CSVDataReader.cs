﻿using System;
using System.Collections.Generic;
using System.Text;
using Data.Connector;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Threading.Tasks;
using System.IO;
using Data.Adaptor;
using System.Text.RegularExpressions;
using System.Linq;

namespace Data.Reader
{
    public class CSVDataReader : DataReader, ICSVDataReader
    {
        IConfiguration configuration;

        public CSVDataReader(IConfiguration _configuration) : base(_configuration) => configuration = _configuration;

        public async Task<bool> ReadStream(Stream stream, string tableName, bool isTableExists, bool replace)
        {
            bool success = false;
            try
            {
                string csvDelimeter = ",";
                var reader = new StreamReader(stream);
                var line = await reader.ReadLineAsync();
                var columnNames = Regex.Split(line, csvDelimeter);
                DataTable dt = new DataTable();
                Dictionary<string, string> columnsWithTypes = new Dictionary<string, string>();
                IDataSourceAdaptor iCsvAdaptor = new CSVAdaptor(new Data.Reader.CSVDataReader(configuration));
                // tableName.Replace(" ", "_") + "_ID
                dt.Columns.Add(tableName.Replace(" ", "_") + "_ID", typeof(int));
                for (int j = 0; j < columnNames.Length; j++)
                {
                    columnsWithTypes.Add(columnNames[j], "VARCHAR(250)");
                    dt.Columns.Add(columnNames[j], typeof(string));
                }

                dt.Columns.Add(tableName.Replace(" ", "_") + "_CreatedDate", typeof(DateTime));

                int i = 0;
                while (!reader.EndOfStream)
                {
                    //line = reader.ReadLine();
                    //var values = line.Split(csvDelimeter.ToCharArray()).ToList();

                    while (!string.IsNullOrEmpty(line = await reader.ReadLineAsync()))
                    {
                        string[] cols = Regex.Split(line, ",");
                        DataRow row = dt.NewRow();

                        for (int j = 0; j < columnNames.Length; j++)
                        {
                            //columnsWithTypes.Add(columnNames[j], "VARCHAR(100)");
                            //dt.Columns.Add("TI", typeof(string));
                            row[j +1] = cols[j];
                        }
                        row[columnNames.Length +1] = DateTime.Now;
                        dt.Rows.Add(row);
                    }

                    i++;
                }

                success =  await iCsvAdaptor.CreatData(tableName, columnsWithTypes, dt, isTableExists, replace);
                return success;
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                Console.WriteLine("Error : " + message);

                return success;
            }
        }
    }
}
