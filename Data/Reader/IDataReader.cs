﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Data.Reader
{
    public interface IDataReader
    {
        Task<bool> CreateData(string tableName, Dictionary<string, string> columns, DataTable dataTable, bool isTableExists, bool replace);
        Task<bool> GetData();
    }
}
