﻿using Data.Connector;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Data.Reader
{
    public class DataReader : IDataReader
    {
        IConfiguration configuration;

        public DataReader(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public async Task<bool> CreateData(string tableName, Dictionary<string, string> columns, DataTable dataTable, bool isTableExists = false, bool replace = false)
        {
            try
            {
                string sColumns = "";
                foreach (KeyValuePair<string, string> entry in columns)
                {
                    // CREATE COLUMNS AND ASSIGN DataTypes.
                    if (string.IsNullOrEmpty(sColumns))
                        sColumns = "[" + entry.Key.Replace(" ", "") + "] VARCHAR (250)";
                    else
                        sColumns = sColumns + ", [" + entry.Key.Replace(" ", "") + "] VARCHAR (250)";
                }

                SQLConnector sqlConnector = new SQLConnector(configuration);
                using (SqlConnection con = sqlConnector.GetConnectionByConnectionName(string.Empty))
                {
                    // CREATE TABLE STRUCTURE USING THE COLUMNS AND TABLE NAME.

                    if (!isTableExists)
                    {
                        string sQuery = null;
                        sQuery = "IF OBJECT_ID('dbo." + tableName.Replace(" ", "_") + "', 'U') IS NULL " +
                            "BEGIN " +
                            "CREATE TABLE [dbo].[" + tableName.Replace(" ", "_") + "](" +
                              //"[ID] INT IDENTITY(1,1) NOT NULL CONSTRAINT pk" +
                                "[" + tableName.Replace(" ", "_") + "_ID] INT IDENTITY(1,1) NOT NULL ," +
                                sColumns + ", " +
                                "[" + tableName.Replace(" ", "_") + "_CreatedDate] DATETIME, " +
                                "CONSTRAINT ['pk_" + tableName + "'] PRIMARY KEY CLUSTERED "
                                + "("
                                + "[" + tableName.Replace(" ", "_") + "_ID] ASC"
                                + ") WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]"
                                + ") ON [PRIMARY]" +
                            " END";

                        await sqlConnector.ExecuteNonQuery(string.Empty, sQuery);
                    }

                    string table = "dbo." + tableName.Replace(" ", "_");

                    //Inserting bulk data to sql table
                    await sqlConnector.InsertBulkData(string.Empty, table, dataTable, isTableExists, replace);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {

            }
        }

        public async Task<bool> GetData()
        {
            throw new NotImplementedException();
        }
    }
}
