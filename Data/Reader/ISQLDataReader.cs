﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Data.Reader
{
    public interface ISQLDataReader :IDataReader
    {
        Task<string> ReadData();
        Task<bool> StoreData(SqlConnection con, string externalSourceTableName, string storingTableName, bool isTableExists, bool replace);
    }
}
