﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Data.Reader
{
    public interface ICSVDataReader : IDataReader
    {
        Task<bool> ReadStream(Stream stream, string tableName, bool tableExists, bool replace);
    }
}
