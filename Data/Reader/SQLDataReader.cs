﻿using Data.Adaptor;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Data.Reader
{
    public class SQLDataReader : DataReader, ISQLDataReader
    {
        IConfiguration configuration;

        public SQLDataReader(IConfiguration _configuration) : base(_configuration) => configuration = _configuration;

        public Task<string> ReadData()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> StoreData(SqlConnection con, string externalSourceTableName, string storingTableName, bool isTableExists, bool replace)
        {
            bool success = false;
            try
            {
                Dictionary<string, string> columnsWithTypes = new Dictionary<string, string>();
                DataSet ds = new DataSet();
                string query = "SELECT * FROM " + externalSourceTableName;
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = query;
                    // assign parameters passed in to the command
                    //foreach (var procParameter in procParameters)
                    //{
                    //    cmd.Parameters.Add(procParameter.Value);
                    //}
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds);
                    }
                }

                DataTable dt = new DataTable();
                if (ds != null)
                {
                    dt = ds.Tables[0];
                    string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();

                    DataColumn Col = dt.Columns.Add(storingTableName.Replace(" ", "_") + "_ID", typeof(int));
                    Col.SetOrdinal(0);
                    for (int j = 0; j < columnNames.Length; j++)
                    {
                        columnsWithTypes.Add(columnNames[j], "VARCHAR(250)");
                    }
                    dt.Columns.Add(storingTableName.Replace(" ", "_") + "_CreatedDate", typeof(DateTime));

                   //IEnumerable<DataRow> rows = dt.Rows.Cast<DataRow>().Where(r => r[storingTableName.Replace(" ", "_") + "_CreatedDate"].ToString() == "");
                   // // Loop through the rows and change the name.
                   // rows.ToList().ForEach(r => r.SetField(storingTableName.Replace(" ", "_") + "_CreatedDate", DateTime.Now));

                    var col = dt.Columns[storingTableName.Replace(" ", "_") + "_CreatedDate"];
                    foreach (DataRow row in dt.Rows)
                    {
                        row[col] = DateTime.Now;
                    }

                    IDataSourceAdaptor iSQLAdaptor = new SQLAdaptor(new SQLDataReader(configuration));
                    success = await iSQLAdaptor.CreatData(storingTableName, columnsWithTypes, dt, isTableExists, replace);                 
                }

                return success;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error : " + ex.Message);
                return success;
            }
        }
    }
}
