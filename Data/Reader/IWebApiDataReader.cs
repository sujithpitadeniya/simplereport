﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Data.Reader
{
    public interface IWebApiDataReader :IDataReader
    {
        Task<string> ReadData();
        Task<bool> ReadServiceData(List<string> parameters, string baseUrl, string tableName, bool isTableExists, bool replace);
    }
}
