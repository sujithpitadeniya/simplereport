﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
   public class BaseEntity :Base
    {
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public string CreatedBy { get; set; }
        public string LastUpdatedBy { get; set; }
    }
}
