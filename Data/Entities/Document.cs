﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Entities
{
   public class Document : BaseEntity
    {
        public int PropertyId { get; set; }
        public string PropertyValue { get; set; }
    }
}
