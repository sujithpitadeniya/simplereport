﻿using Data.Connector;
using Microsoft.Extensions.Configuration;
using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services
{
    public class DataService : IDataService
    {
        private IConfiguration configuration;
        public DataService(IConfiguration _configuration)
        {
            configuration = _configuration;
        }

        public List<string> GetTableNames(string connectionName)
        {
            SQLConnector sqlConnector = new SQLConnector(configuration);
            List<string> tables = new List<string>();
            DataTable dt = sqlConnector.GetTableNames(connectionName);
            foreach (DataRow row in dt.Rows)
            {
                string tablename = (string)row[2];
                tables.Add(tablename);
            }

            return tables;
        }

        public DataTable GetDataTable(string connectionName, string tableName)
        {
            SQLConnector sqlConnector = new SQLConnector(configuration);
            string query = "SELECT * FROM " + tableName;
            DataSet ds = sqlConnector.ExecuteQuery(connectionName, query, new Dictionary<string, SqlParameter>());
            DataTable dt = new DataTable();

            if (ds != null)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }
    }
}
