﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IDataService
    {
        List<string> GetTableNames(string connectionName);
        DataTable GetDataTable(string connectionName, string tableName);
    }
}
