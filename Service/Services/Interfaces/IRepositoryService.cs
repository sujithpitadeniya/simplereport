﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IRepositoryService<T> where T : class
    {
        Task<IQueryable<T>> Get(
           Expression<Func<T, bool>> filter = null,
           Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
           string includeProperties = "");

        Task<T> GetById(Int64 id, string includeProperties = "");

        Task<T> Insert(T entity);

        Task Delete(T entityToDelete);

        Task<T> Update(T entityToUpdate);
    }
}
