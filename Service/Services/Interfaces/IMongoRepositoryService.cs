﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Services.Interfaces
{
    public interface IMongoRepositoryService<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> Get(string collectionName);

        T Get(ObjectId id, string collectionName);

        T Create(string collectionName,T p);

        void Update(ObjectId id, string collectionName, T p);

        void Delete(ObjectId id, string collectionName);
    }
}
