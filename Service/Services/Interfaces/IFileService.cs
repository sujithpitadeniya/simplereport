﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Services.Interfaces
{
    public interface IFileService :IRepositoryService<Document>
    {
        Task<string> StoreFile(string fileName);
        Task<bool> ConvertToObject(List<string> values);
    }
}
