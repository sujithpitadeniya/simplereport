﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Services.Interfaces
{
   public interface IMongoDbFileService : IMongoRepositoryService<BsonDocument>
    {
    }
}
