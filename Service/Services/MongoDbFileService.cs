﻿using Service.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using Microsoft.Extensions.Logging;
using Repository.Repositories.Interfaces;

namespace Service.Services
{
    public class MongoDbFileService : IMongoDbFileService
    {
        private ILogger<IMongoDbFileService> logger;
        private readonly IMongoFileRepository iMongoFileRepository;

        public MongoDbFileService(ILogger<IMongoDbFileService> logger,IMongoFileRepository iMongoFileRepository)
        {
            this.logger = logger;
            this.iMongoFileRepository = iMongoFileRepository;
        }
        public BsonDocument Create(string collectionName, BsonDocument p)
        {
            var bSonDoc = iMongoFileRepository.Create(collectionName, p) ;
            return bSonDoc;
        }

        public IEnumerable<BsonDocument> GetAll()
        {
            var docs = iMongoFileRepository.GetAll();
            return docs;
        }
        public IEnumerable<BsonDocument> Get(string collectionName)
        {
            var docs = iMongoFileRepository.Get(collectionName);
            return docs;
        }

        public BsonDocument Get(ObjectId id, string collectionName)
        {
            var doc = iMongoFileRepository.Get(id, collectionName);
            return doc;
        }

        public void Delete(ObjectId id, string collectionName)
        {
            iMongoFileRepository.Delete(id, collectionName);
        }

        public void Update(ObjectId id, string collectionName, BsonDocument p)
        {
            iMongoFileRepository.Update(id, collectionName, p);
        }
    }
}
