﻿using System;
using System.Collections.Generic;
using System.Text;
using Service.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;
using Repository.Repositories.Interfaces;
using Data.Entities;
using System.Linq;
using System.Linq.Expressions;

namespace Service.Services
{
    public class FileService :IFileService
    {
        private Microsoft.Extensions.Logging.ILogger<IFileService> logger;
        private IFileRepository iFileRepository;

        public FileService(Microsoft.Extensions.Logging.ILogger<IFileService> logger, IFileRepository iFileRepository)
        {
            this.logger = logger;
            this.iFileRepository = iFileRepository;
        }

        public async Task<string> StoreFile(string fileName)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ConvertToObject(List<string> values)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Document>> Get(Expression<Func<Document, bool>> filter = null, Func<IQueryable<Document>,
            IOrderedQueryable<Document>> orderBy = null, string includeProperties = "")
        {
            var documents = await iFileRepository.Get(filter, orderBy, includeProperties);
            return documents;
        }

        public async Task<Document> GetById(long id, string includeProperties = "")
        {
            var document = await iFileRepository.GetById(id, includeProperties);
            return document;
        }

        public async Task<Document> Insert(Document entity)
        {
            var document = await iFileRepository.Insert(entity);
            return document;
        }

        public async Task Delete(Document entityToDelete)
        {
           await iFileRepository.Delete(entityToDelete);
        }

        public async Task<Document> Update(Document entityToUpdate)
        {
            var document = await iFileRepository.Update(entityToUpdate);
            return document;
        }
    }
}
